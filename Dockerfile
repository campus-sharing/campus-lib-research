FROM node:8.12.0

RUN apt-get update -y \
  && apt-get install -y imagemagick \
  && apt-get install -y graphicsmagick \
  && apt-get install -y unoconv

RUN convert -version \
  && gm -version

WORKDIR /app
COPY ["package.json", "yarn.lock", "/app/"]
RUN yarn install

ADD . /app
# RUN yarn build

EXPOSE 3000
CMD ["npm", "start"]