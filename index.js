const fs = require('fs');
const gm = require('gm').subClass({ imageMagick: true });
const { spawn } = require('child_process');
const pdf = require('pdf-parse');

new Promise(resolve => {
  const ltn = spawn('unoconv', ['--listener']);

  ltn.stderr.on('data', data => {
    console.log(`listener Error: ${data}`);
  });

  ltn.on('close', code => {
    console.log(`listener exited with code ${code}`);
  });
  setTimeout(() => {
    resolve();
  }, 2000);
})
  .then(() => {
    return new Promise(resolve => {
      const writerStream = fs.createWriteStream('tmp/test4.pdf');

      const unoconv = spawn('unoconv', [
        '-f',
        'pdf',
        '--stdout',
        'tmp/test4.docx'
      ]);

      unoconv.stdout.pipe(writerStream);

      unoconv.stderr.on('data', data => {
        console.log(`stderr: ${data}`);
      });

      unoconv.on('close', code => {
        console.log(`child process exited with code ${code}`);
        resolve();
      });
    });
  })
  .then(() => {
    return new Promise(resolve => {
      gm('./tmp/test4.pdf')
        .drawText(0, 0, 'watermark watermark watermark watermark', 'SouthEast')
        .density(288)
        .write('./tmp/x.pdf', function(err) {
          if (err) {
            throw err;
          }
          console.log('Add Watermark');
          resolve();
        });
    });
  })
  .then(() => {
    const dataBuffer = fs.readFileSync('./tmp/test1.pdf');
    return pdf(dataBuffer).then(data => {
      console.log(data.numpages);
    })
  })
  .catch(err => {
    console.log(err);
  });
